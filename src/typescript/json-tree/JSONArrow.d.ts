import React from 'react';
import PropTypes from 'prop-types';
declare const JSONArrow: {
    ({ arrowStyle, expanded, nodeType, onPress, styling }: any): React.JSX.Element;
    propTypes: {
        arrowStyle: PropTypes.Requireable<string>;
        expanded: PropTypes.Validator<boolean>;
        nodeType: PropTypes.Validator<string>;
        onPress: PropTypes.Validator<(...args: any[]) => any>;
        styling: PropTypes.Validator<(...args: any[]) => any>;
    };
    defaultProps: {
        arrowStyle: string;
    };
};
export default JSONArrow;
//# sourceMappingURL=JSONArrow.d.ts.map