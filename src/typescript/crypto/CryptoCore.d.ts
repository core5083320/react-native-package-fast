declare const CryptoCore: (_K?: any, _iv?: any) => {
    encrypt: (plainText: string) => Promise<any>;
    decrypt: (ciphertext: string) => Promise<any>;
};
export default CryptoCore;
//# sourceMappingURL=CryptoCore.d.ts.map