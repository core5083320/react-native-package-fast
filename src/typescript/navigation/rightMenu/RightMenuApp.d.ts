import React from 'react';
import { ButtonAppProps, InputAppProps } from '../../cores/Utils';
export type RightMenuAppProps = {
    noneCloseTouchOutside?: any;
    backgroundColor?: any;
    noneAni?: any;
    type?: 'center' | 'bottom';
    title?: string;
    description?: string;
    right?: ButtonAppProps;
    input?: InputAppProps;
    left?: ButtonAppProps;
};
declare const RightMenuApp: React.ForwardRefExoticComponent<Pick<any, string | number | symbol> & React.RefAttributes<unknown>>;
export default RightMenuApp;
//# sourceMappingURL=RightMenuApp.d.ts.map