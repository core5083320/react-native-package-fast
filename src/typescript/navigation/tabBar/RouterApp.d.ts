import React from "react";
import { StyleProp, ViewStyle } from "react-native";
declare const RouterApp: ({ screensApp, screenOptions, initialRouteName, TabIconButton, tabBarStyle, onHandlePressTab, checkAfterChangeTab, }: {
    screensApp?: any;
    screenOptions?: any;
    initialRouteName?: any;
    TabIconButton?: any;
    onHandlePressTab?: any;
    checkAfterChangeTab?: any;
    tabBarStyle?: StyleProp<ViewStyle> | any;
}) => React.JSX.Element;
export default RouterApp;
//# sourceMappingURL=RouterApp.d.ts.map