import React from "react";
import { ButtonAppProps, InputAppProps } from "../../cores/Utils";
export type ParamModalProps = {
    noneCloseTouchOutside?: any;
    backgroundColor?: any;
    noneAni?: any;
    type?: "center" | "bottom";
    title?: string;
    description?: string;
    right?: ButtonAppProps;
    input?: InputAppProps;
    left?: ButtonAppProps;
    extras?: any;
};
declare const ModalSecondStack: React.ForwardRefExoticComponent<Pick<any, string | number | symbol> & React.RefAttributes<unknown>>;
export default ModalSecondStack;
//# sourceMappingURL=ModalSecondStack.d.ts.map