import React from 'react';
import { ViewStyle } from 'react-native';
export declare let SCREENS_APP: any;
export declare let POPUPS_APP: any;
export declare let HEADER_APP: any;
interface NavigationProps {
    screensApp?: any;
    popupsApp?: any;
    headerApp?: any;
    params?: any;
    loggerButton?: any;
    leftMenuData?: any;
    leftMenuStyle?: ViewStyle;
    rightMenuData?: any;
    rightMenuStyle?: ViewStyle;
    screenInit?: any;
    linking?: any;
}
declare const NavigationApp: (props: NavigationProps) => React.JSX.Element;
export declare const BaseScreen: ({ route, navigation }: any) => React.JSX.Element;
export default NavigationApp;
//# sourceMappingURL=NavigationApp.d.ts.map