export default class Navigator {
    [x: string]: any;
    constructor(navigation: any, setHeader: any);
    setHeader(options?: any): void;
    goBack(): void;
}
//# sourceMappingURL=Navigator.d.ts.map