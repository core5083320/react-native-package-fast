import React from "react";
import { StyleProp, ViewStyle } from "react-native";
export type StackAppCustomerProps = {
    tabStyle: StyleProp<ViewStyle>;
    navigation: any;
    children: any;
    renderIcon?: any;
};
declare const StackAppCustomer: React.ForwardRefExoticComponent<StackAppCustomerProps & React.RefAttributes<unknown>>;
export default StackAppCustomer;
//# sourceMappingURL=StackAppCustomer.d.ts.map