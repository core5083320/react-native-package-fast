import React from "react";
import { StackAppCustomerProps } from "./StackAppCustomer";
export declare const StackTab: (props: any) => any;
declare const _default: {
    Container: ({ children, renderIcon, tabStyle, navigation, }: StackAppCustomerProps) => React.JSX.Element;
    Screen: (props: any) => any;
};
export default _default;
//# sourceMappingURL=RouterAppCustomer.d.ts.map