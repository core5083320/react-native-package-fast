import React from "react";
import { ImageAppProps } from "../../cores/Utils";
export interface HeaderPropsType {
    title?: string;
    navigation?: any;
    rightChild?: any;
    leftChild?: any;
    left?: ImageAppProps;
    right?: ImageAppProps;
    line?: any;
    hideHeader?: any;
}
declare const HeaderView: React.ForwardRefExoticComponent<Pick<any, string | number | symbol> & React.RefAttributes<unknown>>;
export default HeaderView;
//# sourceMappingURL=HeaderView.d.ts.map