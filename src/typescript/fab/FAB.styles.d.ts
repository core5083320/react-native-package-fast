export declare const width: number, height: number;
declare const _default: {
    defaultStyle: {
        overflow: "hidden";
        alignItems: "center";
        justifyContent: "center";
        backgroundColor: string;
    };
};
export default _default;
//# sourceMappingURL=FAB.styles.d.ts.map