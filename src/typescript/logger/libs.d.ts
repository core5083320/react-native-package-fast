import React from "react";
export declare const TextAppLog: (props: import("react-native-package-fast").TextAppProps) => React.JSX.Element;
export declare const ViewAppLog: (props: import("react-native-package-fast").ViewAppProps) => React.JSX.Element;
export declare const IconLog: ({ color, size }: any) => React.JSX.Element;
//# sourceMappingURL=libs.d.ts.map