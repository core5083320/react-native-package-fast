import React from 'react';
export declare let copyFunc: (_: any) => void;
export declare let toastFunc: (_: any) => void;
export declare const checkKey: (_cer: any) => Promise<boolean>;
export declare let dataSave: any;
declare const FloatButtonLogger: React.ForwardRefExoticComponent<Pick<any, string | number | symbol> & React.RefAttributes<unknown>>;
export default FloatButtonLogger;
//# sourceMappingURL=FloatButtonLogger.d.ts.map