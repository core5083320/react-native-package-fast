import { Headers, NetworkRequestInfoRow, RequestMethod } from "./types";
declare class NetworkRequestInfo {
    id: string;
    type: string;
    url: string;
    method: RequestMethod;
    status: number;
    dataSent: string;
    responseContentType: string;
    responseSize: number;
    requestHeaders: Headers;
    responseHeaders: Headers;
    response: string;
    responseURL: string;
    responseType: string;
    timeout: number;
    closeReason: string;
    messages: string;
    startTime: number;
    endTime: number;
    gqlOperation: string | any;
    constructor(id: string, type: string, method: RequestMethod, url: string);
    get duration(): number;
    get curlRequest(): string;
    update(values: Partial<NetworkRequestInfo>): void;
    escapeQuotes(value: string): string;
    parseData(data: any): any;
    stringifyFormat(data: any): string;
    toRow(): NetworkRequestInfoRow;
    getRequestBody(replaceEscaped?: boolean): string;
    parseResponseBlob(): Promise<string>;
    getResponseBody(): Promise<string>;
}
export default NetworkRequestInfo;
//# sourceMappingURL=NetworkRequestInfo.d.ts.map