import React from "react";
import NetworkRequestInfo from "../NetworkRequestInfo";
import { NetworkRequestInfoRow } from "../types";
export type RequestListProps = {
    requestsInfo?: NetworkRequestInfoRow[];
    onPressItem?: (item: NetworkRequestInfo["id"]) => void;
    options?: {
        text: string;
        onPress: () => void;
    }[];
    showDetails?: boolean;
    onBack?: any;
    listRequestData?: any;
    mapData?: any;
    initTab?: any;
    filterDataList?: any;
    copyFunc?: any;
    toastFunc?: any;
    showLog?: any;
    hide?: any;
    certification?: any;
    debug?: any;
};
declare const RequestList: React.FC<RequestListProps>;
export declare const BackButton: ({ onPress }: any) => React.JSX.Element;
export default RequestList;
//# sourceMappingURL=RequestList.d.ts.map