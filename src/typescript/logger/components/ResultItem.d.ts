import React from "react";
import { NetworkRequestInfoRow } from "../types";
interface Props {
    request: NetworkRequestInfoRow;
    onPress?: any;
    style?: any;
    requestDetail?: any;
    mapData?: any;
}
export declare const getObjectItem: (input: any) => any;
declare const ResultItem: React.FC<Props>;
export default ResultItem;
//# sourceMappingURL=ResultItem.d.ts.map