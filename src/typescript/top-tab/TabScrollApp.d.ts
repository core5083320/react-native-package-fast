import React from 'react';
type Props = {
    scrollEnabled?: any;
    children: any;
};
type ItemProps = {
    name: any;
    children: any;
};
declare const _default: {
    Container: ({ children, scrollEnabled, ...rest }: Props) => React.JSX.Element;
    Item: ({ children }: ItemProps) => React.JSX.Element;
};
export default _default;
//# sourceMappingURL=TabScrollApp.d.ts.map