import React from 'react';
export declare const TAB_BAR_HEIGHT = 54;
declare const _default: React.MemoExoticComponent<({ horizontal, tabs, onChangeTab, activeTab, scrollValue, style }: any) => React.JSX.Element>;
export default _default;
//# sourceMappingURL=TabBarApp.d.ts.map