declare const _default: {
    container: {
        flex: number;
        justifyContent: "center";
        alignItems: "center";
    };
    layer: {
        justifyContent: "flex-start";
        alignItems: "center";
        position: "absolute";
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
    };
};
export default _default;
//# sourceMappingURL=styles.d.ts.map