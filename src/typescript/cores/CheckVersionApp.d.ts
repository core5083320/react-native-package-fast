import React from 'react';
export declare const VERSION_DATA: {
    metaData: {};
};
declare const CheckVersionApp: ({ hardVisible, app, des, CodePush, certification, }: {
    hardVisible?: boolean | undefined;
    CodePush: any;
    app?: string | undefined;
    des?: string | undefined;
    certification?: string | undefined;
}) => React.JSX.Element | null;
export default CheckVersionApp;
//# sourceMappingURL=CheckVersionApp.d.ts.map