export declare const TYPE_LOG: {
    api: {
        text: string;
        isBold: boolean;
    };
    mqtt: {
        text: string;
        isBold: boolean;
    };
    default: {
        text: string;
    };
};
export declare const initLog: () => void;
//# sourceMappingURL=LogUtils.d.ts.map