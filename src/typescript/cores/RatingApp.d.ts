import React from 'react';
import { ViewAppProps } from 'react-native-package-fast';
type Props = {
    rate?: number;
    size?: number;
    color?: string;
    inactiveColor?: string;
    rigth?: number;
    onRate?: (rate: any) => void;
} & ViewAppProps;
declare const RatingApp: ({ rate, size, color, inactiveColor, onRate, rigth, ...rest }: Props) => React.JSX.Element;
export default RatingApp;
//# sourceMappingURL=RatingApp.d.ts.map