import { ImageProps, ImageStyle, StyleProp, TextInputProps, TextProps, TextStyle, TouchableOpacityProps, ViewProps, ViewStyle } from 'react-native';
import { FastImageProps } from 'react-native-fast-image';
export declare const stylesCustom: CustomStylesType & GlobalStylesType;
export declare const setGlobalStyles: ({ scale, colors, fonts, spaces, buttons, inputs }: any) => void;
interface ByPassProps {
    flex?: number;
    width?: any;
    height?: any;
    backgroundColor?: string;
    color?: string;
    minHeight?: number;
    minWidth?: number;
    radius?: number;
    numberOfLines?: number;
    lineHeight?: number;
    fontSize?: number;
    borderWidth?: number;
    borderColor?: string;
    bg?: string;
}
export declare const parseStyles: ({ passStyle, styles, props }: any) => any[];
interface CommonProps extends ByPassProps {
    dev?: boolean;
    topLeft?: any;
    center?: boolean;
    centerItem?: boolean;
    centerContent?: boolean;
    ani?: any;
    size?: any;
    ratio?: any;
    circle?: any;
    Black?: boolean;
    Gray?: boolean;
    White?: boolean;
    Yellow?: boolean;
    Blue?: boolean;
    Red?: boolean;
    Green?: boolean;
    'radius-'?: boolean;
    'border-'?: boolean;
    'border-b-'?: boolean;
    'p-'?: boolean;
    'm-'?: boolean;
    'L-'?: boolean;
    'R-'?: boolean;
    'T-'?: boolean;
    'B-'?: boolean;
    'X-'?: boolean;
    'Y-'?: boolean;
    'l-'?: boolean;
    'r-'?: boolean;
    't-'?: boolean;
    'b-'?: boolean;
    'x-'?: boolean;
    'y-'?: boolean;
    'pt-s'?: boolean;
    'pt-m'?: boolean;
    'pt-l'?: boolean;
    'pt-xl'?: boolean;
    'pt-xxl'?: boolean;
    'pt-xxxl'?: boolean;
    'pb-s'?: boolean;
    'pb-m'?: boolean;
    'pb-l'?: boolean;
    'pb-xl'?: boolean;
    'pb-xxl'?: boolean;
    'pb-xxxl'?: boolean;
    'pl-s'?: boolean;
    'pl-m'?: boolean;
    'pl-l'?: boolean;
    'pl-xl'?: boolean;
    'pl-xxl'?: boolean;
    'pl-xxxl'?: boolean;
    'pr-s'?: boolean;
    'pr-m'?: boolean;
    'pr-l'?: boolean;
    'pr-xl'?: boolean;
    'pr-xxl'?: boolean;
    'pr-xxxl'?: boolean;
    'mt-m'?: boolean;
    'mt-l'?: boolean;
    'mt-xl'?: boolean;
    'mt-xxl'?: boolean;
    'mt-xxxl'?: boolean;
    'mb-s'?: boolean;
    'mb-m'?: boolean;
    'mb-l'?: boolean;
    'mb-xl'?: boolean;
    'mb-xxl'?: boolean;
    'mb-xxxl'?: boolean;
    'ml-s'?: boolean;
    'ml-m'?: boolean;
    'ml-l'?: boolean;
    'ml-xl'?: boolean;
    'ml-xxl'?: boolean;
    'ml-xxxl'?: boolean;
    'mr-s'?: boolean;
    'mr-m'?: boolean;
    'mr-l'?: boolean;
    'mr-xl'?: boolean;
    'mr-xxl'?: boolean;
    'mr-xxxl'?: boolean;
}
export type ViewAppProps = ViewProps & CommonProps & {
    row?: any;
    wrap?: any;
    absFill?: any;
};
export interface TouchAppProps extends TouchableOpacityProps, CommonProps {
    row?: any;
    wrap?: any;
}
export declare const ViewAppStyles: {
    absFill: {
        position: "absolute";
        top: number;
        left: number;
        right: number;
        bottom: number;
    };
    row: {
        flexDirection: "row";
    };
    wrap: {
        flexWrap: "wrap";
    };
    topLeft: {
        position: "absolute";
        top: number;
        left: number;
    };
    center: {
        justifyContent: "center";
        alignItems: "center";
    };
    centerItem: {
        alignItems: "center";
    };
    centerContent: {
        justifyContent: "center";
    };
    'pt-s': {
        paddingTop: number;
    };
    'pt-m': {
        paddingTop: number;
    };
    'pt-l': {
        paddingTop: number;
    };
    'pt-xl': {
        paddingTop: number;
    };
    'pt-xxl': {
        paddingTop: number;
    };
    'pt-xxxl': {
        paddingTop: number;
    };
    'pb-s': {
        paddingBottom: number;
    };
    'pb-m': {
        paddingBottom: number;
    };
    'pb-l': {
        paddingBottom: number;
    };
    'pb-xl': {
        paddingBottom: number;
    };
    'pb-xxl': {
        paddingBottom: number;
    };
    'pb-xxxl': {
        paddingBottom: number;
    };
    'pl-s': {
        paddingLeft: number;
    };
    'pl-m': {
        paddingLeft: number;
    };
    'pl-l': {
        paddingLeft: number;
    };
    'pl-xl': {
        paddingLeft: number;
    };
    'pl-xxl': {
        paddingLeft: number;
    };
    'pl-xxxl': {
        paddingLeft: number;
    };
    'pr-s': {
        paddingRight: number;
    };
    'pr-m': {
        paddingRight: number;
    };
    'pr-l': {
        paddingRight: number;
    };
    'pr-xl': {
        paddingRight: number;
    };
    'pr-xxl': {
        paddingRight: number;
    };
    'pr-xxxl': {
        paddingRight: number;
    };
    'mt-s': {
        marginTop: number;
    };
    'mt-m': {
        marginTop: number;
    };
    'mt-l': {
        marginTop: number;
    };
    'mt-xl': {
        marginTop: number;
    };
    'mt-xxl': {
        marginTop: number;
    };
    'mt-xxxl': {
        marginTop: number;
    };
    'mb-s': {
        marginBottom: number;
    };
    'mb-m': {
        marginBottom: number;
    };
    'mb-l': {
        marginBottom: number;
    };
    'mb-xl': {
        marginBottom: number;
    };
    'mb-xxl': {
        marginBottom: number;
    };
    'mb-xxxl': {
        marginBottom: number;
    };
    'ml-s': {
        marginLeft: number;
    };
    'ml-m': {
        marginLeft: number;
    };
    'ml-l': {
        marginLeft: number;
    };
    'ml-xl': {
        marginLeft: number;
    };
    'ml-xxl': {
        marginLeft: number;
    };
    'ml-xxxl': {
        marginLeft: number;
    };
    'mr-s': {
        marginRight: number;
    };
    'mr-m': {
        marginRight: number;
    };
    'mr-l': {
        marginRight: number;
    };
    'mr-xl': {
        marginRight: number;
    };
    'mr-xxl': {
        marginRight: number;
    };
    'mr-xxxl': {
        marginRight: number;
    };
    Black: {
        backgroundColor: string;
    };
    Gray: {
        backgroundColor: string;
    };
    Green: {
        backgroundColor: string;
    };
    Red: {
        backgroundColor: string;
    };
    Blue: {
        backgroundColor: string;
    };
    White: {
        backgroundColor: string;
    };
    Yellow: {
        backgroundColor: string;
    };
};
export type TextAppProps = TextProps & CommonProps & {
    h1?: boolean;
    h2?: boolean;
    h3?: boolean;
    h4?: boolean;
    h5?: boolean;
    h6?: boolean;
    h7?: boolean;
    header?: boolean;
    black?: boolean;
    white?: boolean;
    yellow?: boolean;
    orange?: boolean;
    red?: boolean;
    green?: boolean;
    blue?: boolean;
    gray?: boolean;
    bold?: boolean;
    medium?: boolean;
    regular?: boolean;
    line?: boolean;
    alignCenter?: boolean;
    left?: boolean;
    alignVerCenter?: boolean;
    title?: String;
    children?: any;
    hardHeight?: boolean;
};
export declare const TextAppStyles: {
    default: {};
    black: {
        color: string;
    };
    gray: {
        color: string;
    };
    green: {
        color: string;
    };
    red: {
        color: string;
    };
    blue: {
        color: string;
    };
    white: {
        color: string;
    };
    yellow: {
        color: string;
    };
    line: {
        textDecorationLine: "underline";
    };
    alignVerCenter: {
        textAlignVertical: "center";
    };
    alignCenter: {
        textAlign: "center";
    };
    left: {
        textAlign: "left";
    };
    center: {
        extAlign: string;
        textAlignVertical: "center";
    };
    topLeft: {
        position: "absolute";
        top: number;
        left: number;
    };
    centerItem: {
        alignItems: "center";
    };
    centerContent: {
        justifyContent: "center";
    };
    'pt-s': {
        paddingTop: number;
    };
    'pt-m': {
        paddingTop: number;
    };
    'pt-l': {
        paddingTop: number;
    };
    'pt-xl': {
        paddingTop: number;
    };
    'pt-xxl': {
        paddingTop: number;
    };
    'pt-xxxl': {
        paddingTop: number;
    };
    'pb-s': {
        paddingBottom: number;
    };
    'pb-m': {
        paddingBottom: number;
    };
    'pb-l': {
        paddingBottom: number;
    };
    'pb-xl': {
        paddingBottom: number;
    };
    'pb-xxl': {
        paddingBottom: number;
    };
    'pb-xxxl': {
        paddingBottom: number;
    };
    'pl-s': {
        paddingLeft: number;
    };
    'pl-m': {
        paddingLeft: number;
    };
    'pl-l': {
        paddingLeft: number;
    };
    'pl-xl': {
        paddingLeft: number;
    };
    'pl-xxl': {
        paddingLeft: number;
    };
    'pl-xxxl': {
        paddingLeft: number;
    };
    'pr-s': {
        paddingRight: number;
    };
    'pr-m': {
        paddingRight: number;
    };
    'pr-l': {
        paddingRight: number;
    };
    'pr-xl': {
        paddingRight: number;
    };
    'pr-xxl': {
        paddingRight: number;
    };
    'pr-xxxl': {
        paddingRight: number;
    };
    'mt-s': {
        marginTop: number;
    };
    'mt-m': {
        marginTop: number;
    };
    'mt-l': {
        marginTop: number;
    };
    'mt-xl': {
        marginTop: number;
    };
    'mt-xxl': {
        marginTop: number;
    };
    'mt-xxxl': {
        marginTop: number;
    };
    'mb-s': {
        marginBottom: number;
    };
    'mb-m': {
        marginBottom: number;
    };
    'mb-l': {
        marginBottom: number;
    };
    'mb-xl': {
        marginBottom: number;
    };
    'mb-xxl': {
        marginBottom: number;
    };
    'mb-xxxl': {
        marginBottom: number;
    };
    'ml-s': {
        marginLeft: number;
    };
    'ml-m': {
        marginLeft: number;
    };
    'ml-l': {
        marginLeft: number;
    };
    'ml-xl': {
        marginLeft: number;
    };
    'ml-xxl': {
        marginLeft: number;
    };
    'ml-xxxl': {
        marginLeft: number;
    };
    'mr-s': {
        marginRight: number;
    };
    'mr-m': {
        marginRight: number;
    };
    'mr-l': {
        marginRight: number;
    };
    'mr-xl': {
        marginRight: number;
    };
    'mr-xxl': {
        marginRight: number;
    };
    'mr-xxxl': {
        marginRight: number;
    };
    Black: {
        backgroundColor: string;
    };
    Gray: {
        backgroundColor: string;
    };
    Green: {
        backgroundColor: string;
    };
    Red: {
        backgroundColor: string;
    };
    Blue: {
        backgroundColor: string;
    };
    White: {
        backgroundColor: string;
    };
    Yellow: {
        backgroundColor: string;
    };
};
export interface ButtonAppProps extends TouchAppProps, CommonProps {
    title?: String;
    children?: any;
    propsText?: TextAppProps;
    leftStyle?: ImageStyle;
    rightStyle?: ImageStyle;
    size?: 'small' | 'medium' | 'big';
    left?: any;
    right?: any;
    type?: 'primary' | 'sub' | 'border' | 'border' | 'border' | 'disabled';
}
export declare const ButtonAppStyles: {
    absFill: {
        position: "absolute";
        top: number;
        left: number;
        right: number;
        bottom: number;
    };
    row: {
        flexDirection: "row";
    };
    wrap: {
        flexWrap: "wrap";
    };
    topLeft: {
        position: "absolute";
        top: number;
        left: number;
    };
    center: {
        justifyContent: "center";
        alignItems: "center";
    };
    centerItem: {
        alignItems: "center";
    };
    centerContent: {
        justifyContent: "center";
    };
    'pt-s': {
        paddingTop: number;
    };
    'pt-m': {
        paddingTop: number;
    };
    'pt-l': {
        paddingTop: number;
    };
    'pt-xl': {
        paddingTop: number;
    };
    'pt-xxl': {
        paddingTop: number;
    };
    'pt-xxxl': {
        paddingTop: number;
    };
    'pb-s': {
        paddingBottom: number;
    };
    'pb-m': {
        paddingBottom: number;
    };
    'pb-l': {
        paddingBottom: number;
    };
    'pb-xl': {
        paddingBottom: number;
    };
    'pb-xxl': {
        paddingBottom: number;
    };
    'pb-xxxl': {
        paddingBottom: number;
    };
    'pl-s': {
        paddingLeft: number;
    };
    'pl-m': {
        paddingLeft: number;
    };
    'pl-l': {
        paddingLeft: number;
    };
    'pl-xl': {
        paddingLeft: number;
    };
    'pl-xxl': {
        paddingLeft: number;
    };
    'pl-xxxl': {
        paddingLeft: number;
    };
    'pr-s': {
        paddingRight: number;
    };
    'pr-m': {
        paddingRight: number;
    };
    'pr-l': {
        paddingRight: number;
    };
    'pr-xl': {
        paddingRight: number;
    };
    'pr-xxl': {
        paddingRight: number;
    };
    'pr-xxxl': {
        paddingRight: number;
    };
    'mt-s': {
        marginTop: number;
    };
    'mt-m': {
        marginTop: number;
    };
    'mt-l': {
        marginTop: number;
    };
    'mt-xl': {
        marginTop: number;
    };
    'mt-xxl': {
        marginTop: number;
    };
    'mt-xxxl': {
        marginTop: number;
    };
    'mb-s': {
        marginBottom: number;
    };
    'mb-m': {
        marginBottom: number;
    };
    'mb-l': {
        marginBottom: number;
    };
    'mb-xl': {
        marginBottom: number;
    };
    'mb-xxl': {
        marginBottom: number;
    };
    'mb-xxxl': {
        marginBottom: number;
    };
    'ml-s': {
        marginLeft: number;
    };
    'ml-m': {
        marginLeft: number;
    };
    'ml-l': {
        marginLeft: number;
    };
    'ml-xl': {
        marginLeft: number;
    };
    'ml-xxl': {
        marginLeft: number;
    };
    'ml-xxxl': {
        marginLeft: number;
    };
    'mr-s': {
        marginRight: number;
    };
    'mr-m': {
        marginRight: number;
    };
    'mr-l': {
        marginRight: number;
    };
    'mr-xl': {
        marginRight: number;
    };
    'mr-xxl': {
        marginRight: number;
    };
    'mr-xxxl': {
        marginRight: number;
    };
    Black: {
        backgroundColor: string;
    };
    Gray: {
        backgroundColor: string;
    };
    Green: {
        backgroundColor: string;
    };
    Red: {
        backgroundColor: string;
    };
    Blue: {
        backgroundColor: string;
    };
    White: {
        backgroundColor: string;
    };
    Yellow: {
        backgroundColor: string;
    };
};
export type InputAppProps = TextInputProps & CommonProps & {
    borderType?: any;
    minHeight?: number;
    disabled?: boolean;
    left?: any;
    right?: any;
    leftView?: any;
    rightView?: any;
    container?: any;
    sizeIcon?: number;
    leftStyle?: any;
    rightStyle?: any;
    onClear?: any;
    onLeftPress?: any;
};
export declare const InputAppStyles: {
    borderType: {
        paddingHorizontal: number;
        borderWidth: number;
        borderColor: string;
        height: number;
        justifyContent: "center";
        borderRadius: number;
    };
    topLeft: {
        position: "absolute";
        top: number;
        left: number;
    };
    center: {
        justifyContent: "center";
        alignItems: "center";
    };
    centerItem: {
        alignItems: "center";
    };
    centerContent: {
        justifyContent: "center";
    };
    'pt-s': {
        paddingTop: number;
    };
    'pt-m': {
        paddingTop: number;
    };
    'pt-l': {
        paddingTop: number;
    };
    'pt-xl': {
        paddingTop: number;
    };
    'pt-xxl': {
        paddingTop: number;
    };
    'pt-xxxl': {
        paddingTop: number;
    };
    'pb-s': {
        paddingBottom: number;
    };
    'pb-m': {
        paddingBottom: number;
    };
    'pb-l': {
        paddingBottom: number;
    };
    'pb-xl': {
        paddingBottom: number;
    };
    'pb-xxl': {
        paddingBottom: number;
    };
    'pb-xxxl': {
        paddingBottom: number;
    };
    'pl-s': {
        paddingLeft: number;
    };
    'pl-m': {
        paddingLeft: number;
    };
    'pl-l': {
        paddingLeft: number;
    };
    'pl-xl': {
        paddingLeft: number;
    };
    'pl-xxl': {
        paddingLeft: number;
    };
    'pl-xxxl': {
        paddingLeft: number;
    };
    'pr-s': {
        paddingRight: number;
    };
    'pr-m': {
        paddingRight: number;
    };
    'pr-l': {
        paddingRight: number;
    };
    'pr-xl': {
        paddingRight: number;
    };
    'pr-xxl': {
        paddingRight: number;
    };
    'pr-xxxl': {
        paddingRight: number;
    };
    'mt-s': {
        marginTop: number;
    };
    'mt-m': {
        marginTop: number;
    };
    'mt-l': {
        marginTop: number;
    };
    'mt-xl': {
        marginTop: number;
    };
    'mt-xxl': {
        marginTop: number;
    };
    'mt-xxxl': {
        marginTop: number;
    };
    'mb-s': {
        marginBottom: number;
    };
    'mb-m': {
        marginBottom: number;
    };
    'mb-l': {
        marginBottom: number;
    };
    'mb-xl': {
        marginBottom: number;
    };
    'mb-xxl': {
        marginBottom: number;
    };
    'mb-xxxl': {
        marginBottom: number;
    };
    'ml-s': {
        marginLeft: number;
    };
    'ml-m': {
        marginLeft: number;
    };
    'ml-l': {
        marginLeft: number;
    };
    'ml-xl': {
        marginLeft: number;
    };
    'ml-xxl': {
        marginLeft: number;
    };
    'ml-xxxl': {
        marginLeft: number;
    };
    'mr-s': {
        marginRight: number;
    };
    'mr-m': {
        marginRight: number;
    };
    'mr-l': {
        marginRight: number;
    };
    'mr-xl': {
        marginRight: number;
    };
    'mr-xxl': {
        marginRight: number;
    };
    'mr-xxxl': {
        marginRight: number;
    };
    Black: {
        backgroundColor: string;
    };
    Gray: {
        backgroundColor: string;
    };
    Green: {
        backgroundColor: string;
    };
    Red: {
        backgroundColor: string;
    };
    Blue: {
        backgroundColor: string;
    };
    White: {
        backgroundColor: string;
    };
    Yellow: {
        backgroundColor: string;
    };
};
export interface ImageFastAppProps extends FastImageProps, CommonProps {
    contain?: boolean;
    cover?: boolean;
    stretch?: boolean;
    size?: number;
    ratio?: any;
    color?: string;
}
export type ImageAppProps = ImageProps & CommonProps & {
    contain?: boolean;
    cover?: boolean;
    stretch?: boolean;
    size?: number;
    ratio?: any;
    color?: string;
    source?: any;
};
export declare const ImageAppStyles: {
    contain: {
        resizeMode: "contain";
    };
    cover: {
        resizeMode: "cover";
    };
    stretch: {
        resizeMode: "stretch";
    };
    topLeft: {
        position: "absolute";
        top: number;
        left: number;
    };
    center: {
        justifyContent: "center";
        alignItems: "center";
    };
    centerItem: {
        alignItems: "center";
    };
    centerContent: {
        justifyContent: "center";
    };
    'pt-s': {
        paddingTop: number;
    };
    'pt-m': {
        paddingTop: number;
    };
    'pt-l': {
        paddingTop: number;
    };
    'pt-xl': {
        paddingTop: number;
    };
    'pt-xxl': {
        paddingTop: number;
    };
    'pt-xxxl': {
        paddingTop: number;
    };
    'pb-s': {
        paddingBottom: number;
    };
    'pb-m': {
        paddingBottom: number;
    };
    'pb-l': {
        paddingBottom: number;
    };
    'pb-xl': {
        paddingBottom: number;
    };
    'pb-xxl': {
        paddingBottom: number;
    };
    'pb-xxxl': {
        paddingBottom: number;
    };
    'pl-s': {
        paddingLeft: number;
    };
    'pl-m': {
        paddingLeft: number;
    };
    'pl-l': {
        paddingLeft: number;
    };
    'pl-xl': {
        paddingLeft: number;
    };
    'pl-xxl': {
        paddingLeft: number;
    };
    'pl-xxxl': {
        paddingLeft: number;
    };
    'pr-s': {
        paddingRight: number;
    };
    'pr-m': {
        paddingRight: number;
    };
    'pr-l': {
        paddingRight: number;
    };
    'pr-xl': {
        paddingRight: number;
    };
    'pr-xxl': {
        paddingRight: number;
    };
    'pr-xxxl': {
        paddingRight: number;
    };
    'mt-s': {
        marginTop: number;
    };
    'mt-m': {
        marginTop: number;
    };
    'mt-l': {
        marginTop: number;
    };
    'mt-xl': {
        marginTop: number;
    };
    'mt-xxl': {
        marginTop: number;
    };
    'mt-xxxl': {
        marginTop: number;
    };
    'mb-s': {
        marginBottom: number;
    };
    'mb-m': {
        marginBottom: number;
    };
    'mb-l': {
        marginBottom: number;
    };
    'mb-xl': {
        marginBottom: number;
    };
    'mb-xxl': {
        marginBottom: number;
    };
    'mb-xxxl': {
        marginBottom: number;
    };
    'ml-s': {
        marginLeft: number;
    };
    'ml-m': {
        marginLeft: number;
    };
    'ml-l': {
        marginLeft: number;
    };
    'ml-xl': {
        marginLeft: number;
    };
    'ml-xxl': {
        marginLeft: number;
    };
    'ml-xxxl': {
        marginLeft: number;
    };
    'mr-s': {
        marginRight: number;
    };
    'mr-m': {
        marginRight: number;
    };
    'mr-l': {
        marginRight: number;
    };
    'mr-xl': {
        marginRight: number;
    };
    'mr-xxl': {
        marginRight: number;
    };
    'mr-xxxl': {
        marginRight: number;
    };
    Black: {
        backgroundColor: string;
    };
    Gray: {
        backgroundColor: string;
    };
    Green: {
        backgroundColor: string;
    };
    Red: {
        backgroundColor: string;
    };
    Blue: {
        backgroundColor: string;
    };
    White: {
        backgroundColor: string;
    };
    Yellow: {
        backgroundColor: string;
    };
};
export type FloatButtonProps = ViewAppProps & {
    children?: any;
    style?: any;
    actions?: any;
    iconView?: any;
    styleActions?: StyleProp<ViewStyle>;
    container?: StyleProp<ViewStyle>;
};
export type LoadingAppProps = ViewAppProps & {
    type?: 'ball' | 'bar' | 'dot' | 'ind' | 'mater' | 'pac_man' | 'pulse' | 'skype' | 'ui' | 'wave';
    count?: number;
    color?: String;
    animationDuration?: number;
    waveMode?: 'fill' | 'outline';
    waveFactor?: any;
    container?: ViewAppProps;
    size?: number;
};
type SpaceType = {
    s: number;
    m: number;
    l: number;
    xl: number;
    xxl: number;
    xxxl: number;
};
type ColorType = {
    black: string;
    blue: string;
    white: string;
    yellow: string;
    red: string;
    gray: string;
    green: string;
};
export type FontType = {
    h1: TextStyle;
    h2: TextStyle;
    h3: TextStyle;
    h4: TextStyle;
    h5: TextStyle;
    h6: TextStyle;
    h7: TextStyle;
    bold: TextStyle;
    regular: TextStyle;
    medium: TextStyle;
};
type ButtonItemType = ViewStyle & {
    textStyles?: TextStyle;
    iconStyles?: ImageStyle;
};
export type ButtonType = {
    big?: ButtonItemType;
    medium?: ButtonItemType;
    small?: ButtonItemType;
    primary?: ButtonItemType;
    sub?: ButtonItemType;
    border?: ButtonItemType;
    disabled?: ButtonItemType;
};
export type InputItemType = InputAppProps & {};
export type InputType = {
    default: InputItemType;
    disabled?: InputItemType;
    focus?: InputItemType;
};
type GlobalStylesType = {
    colors: ColorType;
    spaces: SpaceType;
    lineScale: number;
};
type CustomStylesType = {
    fonts: FontType;
    buttons: ButtonType;
    input: InputType;
};
export {};
//# sourceMappingURL=Utils.d.ts.map