import React from 'react';
export declare let DATA_CACHE_STORE: {
    key: string;
    data: {};
};
export declare const getContextStore: () => undefined;
declare const _default: React.MemoExoticComponent<(props: any) => React.JSX.Element>;
export default _default;
//# sourceMappingURL=StoresProviderApp.d.ts.map