import React from 'react';
import { InputAppProps } from './Utils';
export declare const InputAppCore: (props: InputAppProps) => React.JSX.Element;
declare const _default: React.MemoExoticComponent<(props: InputAppProps) => React.JSX.Element>;
export default _default;
//# sourceMappingURL=InputApp.d.ts.map