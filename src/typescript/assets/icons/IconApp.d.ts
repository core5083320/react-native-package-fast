declare const IconApp: {
    ic_exit: any;
    ic_rate_empty: any;
    ic_rate_full: any;
    ic_rate_half: any;
    ic_toast_info: any;
    ic_toast_success: any;
    ic_toast_warning: any;
    ic_toast_error: any;
    ic_close: any;
};
export default IconApp;
//# sourceMappingURL=IconApp.d.ts.map