import React from "react";
import { ViewAppProps } from "../cores/Utils";
export type SkeletonAppProps = {
    type?: "line" | "media";
    children?: any;
    duration?: any;
    size?: any;
    isRound?: any;
    animation?: any;
} & ViewAppProps;
declare const _default: {
    Line: (props: SkeletonAppProps) => React.JSX.Element;
    Media: (props: SkeletonAppProps) => React.JSX.Element;
};
export default _default;
//# sourceMappingURL=SkeletonApp.d.ts.map