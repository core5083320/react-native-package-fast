# react-native-package-fast

react-native-package-fast

## Installation

```sh
npm install react-native-package-fast
yarn add react-native-package-fast
```

## Usage

```js
<ViewApp Hoz-10 flex={1} Top-2 Bot-2 bot-2 top-2 center/>
<TextApp Hoz-10></TextApp>
<ImageApp source={url} size={20} content/>
<ImageFastApp/>
<ButtonApp>text</ButtonApp>
```

## EXAMPLE

| componet      | Param      | Style                 |
| ------------- | ---------- | --------------------- |
| All Component | Hoz-10     | marginHorizontal:10   |
|               | hoz-10     | paddingHorizontal:10  |
|               | Ver-10     | marginVertical:10     |
|               | ver-10     | paddingVertical:10    |
|               | Bot-10     | marginBottom:10       |
|               | bot-10     | paddingBottom:10      |
|               | Top-10     | marginTop:10          |
|               | top-10     | paddingTop:10         |
|               | Left-10    | marginLeft:10         |
|               | left-10    | paddingLeft:10        |
|               | Right-10   | marginRight:10        |
|               | right-10   | paddingRight:10       |
|               | padding-10 | padding:10            |
|               | margin-10  | margin:10             |
|               | flex={1}   | style={{flex:1}}      |
|               | width={1}  | style={{width:10}}    |
|               | height={1} | style={{height:1}}    |
| TextApp       | radius-10  | borderRadius:10       |
|               | border-10  | borderWidth:10        |
|               | font-10    | fontSize:10           |
|               | line-10    | lineHeight:10         |
|               | h1         | fontSize:20           |
|               | h2         | fontSize:18           |
|               | h3         | fontSize:16           |
|               | h4         | fontSize:14           |
|               | h5         | fontSize:12           |
|               | h6         | fontSize:10           |
|               | h7         | fontSize:9            |
|               | header     | fontSize:24           |
| ImageApp      | contain    | resizeMode: 'contain' |
|               | cover      | resizeMode: 'cover'   |
|               | stretch    | resizeMode: 'stretch' |
| ButtonApp     |
